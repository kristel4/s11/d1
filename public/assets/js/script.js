//Comments

//Comments are sections in the code that is not read by the language
//It is for the user's to know what the code does

//In JS, ther are 2 types of comments:
/*
	1. The single line comment, denote by ??
	2. The multi line comment, denote by slash & a asterisk

	*/

/*

Syntax & statements

Syntax contains the rules needed to create a statement in a programming language
Statements are the instructions we tell the application to perform.

Statements end w/ a semicolon(;) as best practice

*/

/*

Variables & constants

Variables and constant are conatiner for data that can used to store info and retrieve or manipulate in the futue

Variables can contain values that can channge as the program runs
Constants contain values that cannot change as the programs runs

To create a variable, we use the "let" keywoord (keywords are special words ina programming language)
To creat a constant, we use the "const" keyword
*/

let productName = "Desktop computer"; //"desktop computer is string, strings usually have ' or " to indicate they are strings
let productPrice = 18999; //18999 is a number, numer do not need ' or "' to indiccate they are numbers
const PI = 3.1416; //PI is a const because the value of PI can never change

/*

Rules in naming variables & constants:
1. Variable names starts w/lowercase letters, for multiple words, we use camelCase
2. Variable names should be descriptive of what values it can contain

*/

/*

Console - is a part of a browser wherein outputs can be desplayed. It can be accessed via the console tab in any browser

To output a value in a console, we use the console.log function

*/

console.log(productName);//console is an object in JS and .log is a function that allows writing of the output
console.log(productPrice);
console.log(PI);

console.log("Hello World"); //We can directly output any value
console.log(12345);
console.log("I am selling a " + productName);

/* Data Types -are type of data a variable can handle
1. String-a combination of alphanumeric value ex. Terence, password1234
2. Number- positive, negative or decimal values ex. 100, -9, 3.1416
3. Boolean-truth value ex. true or false
4. BigInt* (not used often)
5. Symbol* (not used often)
6. Undefined-only appears if a variable was not assignes any value
7. Null-the variable is intentionally left to have a null value
8. Object-combination of data with key-values

*/

//String
let fullName = "Brandon B. Brandon";
let schoolName = "Zuitt Coding Bootcamp";
let userName = "brandon00"

//Number
let age = 28;
let numberOfPets = 5;
let desiredGrade = 98.5;

//Boolean
let isSingle = true; //no need for "" if Boolean
let hasEaten = false;

//Undefined
let petName; //undefined because we didn't assign any value

//Null
let grandChildName = null; //you don't have any grandchildren yet

//Object
let person = {
	firstName : "Jobert", //the comma indicates that there are still more properties after this line
	lastName : "Boyd",
	age : 15,
	petName : "Whitney" // you dont need a comma for the last property
}; //semicolon is after the closing curly brace


/*
Operators - allow us to perform operations or evaluate results
There are 5 main types of operators
1. Assignment -
2. Arithmetic
3. Comparison
4. Relational
5. Logical
*/

//Assignment operator uses the = symbol -> it assigns a value to a variable
let num1 = 28;
let num2 = 75;

//Arithmetic operator has 5 oprations
let sum = num1 + num2;
console.log(sum);
let difference = num1 - num2;
console.log(difference);
let product = num1 * num2;
console.log(product);
let quotient = num2 / num1;
console.log(quotient);
let remainder = num2 % num1; //% is called the modulo operator and gets the remainder of 2 numbers
console.log(remainder);

//you can actually combine the assignment & arithmethic operators
let num3 = 17;
//num3 = num3 - 4;
//the line num3 = num3 - 4 can be reduced can into this line:
num3 -=4; //this is the same as num3 =num3 - 4; this is a shorthand version

//Special arithmetic operator ++ and -- (increment & decrement +1 or -1)
let num4 = 5;
num4++;
console.log(num4);
//Research homework: there is a diff bet num++ and ++num.

//Comparison & relational
//comparison compares 2 values if they are equal or not
//==, ===, !=, !==
let numA = 65;
let numB = 65;
console.log(numA == numB); //true

let statement1 = "true"; //String
let statement2 = true; //Boolean
let statement3 = statement1 == statement2;
// console.log(statement3);
let statement4 = statement1 === statement2;
// console.log(statement4);


//Relational compare 2 numbers if they are equal or not
//>, <, >=, <=
let numC = 25;
let numD = 45;

console.log(numC > numD);
console.log(numC < numD);
console.log(numC >= numD);

//Logical - compares 2 boolean values
//AND (&&), OR (||), NOT (!)
let isTall = false;
let isDark = true;
let isHandsome = true;
let didPassStandard = isTall && isDark; //Both conditions should be true
console.log(didPassStandard); //false

let didPassLowerStandard = isTall || isDark; //Needs only atleast 1 condition to be true
console.log(didPassLowerStandard)//true

console.log(!isTall); //true -> ! reverse the value
//note you can combine comparison, relational & logical operators
let result = isTall || (isDark && isHandsome);
console.log(result)
